package com.jimenezzj.bibloteca.models;

import java.util.Objects;

/**
 * Es la representacion de un usuario administrativo en bibloi app
 *
 * @author Jonatham Jimenez Zambrama
 * @version 1.0
 */
public class Administrativo extends Persona {

    private String nombramiento;
    private int horasSemana;

    /**
     * Crea una representacion de un usuario administrativo en la aplicacion
     *
     * @param idCard         identificaion del usuario
     * @param nombre         nombre del usuario
     * @param primerApellido primer apellido del usuario
     * @param nombramiento   el nombramiento del usuario
     * @param horasSemana    horas por semana que labora el usuario
     */
    public Administrativo(String idCard, String nombre, String primerApellido,
                          String nombramiento, int horasSemana) {
        super(idCard, nombre, primerApellido);
        this.nombramiento = nombramiento;
        this.horasSemana = horasSemana;
    }

    /**
     * Crea un objeto que represta un usuario administrativo con los valores sin inicializar
     */
    public Administrativo() {

    }

    /**
     * Crea un representacion de tipo String del usuario administrativo
     *
     * @return representacion de tipo String del usuario administrativo
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "Administrativo [" + getParentString +
                "nombramiento=" + nombramiento +
                ", horasSemana=" + horasSemana +
                ']';
    }

    /**
     * Compara dos objetos de usuario administrativo para verificar si son iguales,
     * comparando sus referencia, atributos e instancias
     *
     * @param o objetto con el que se realizara la compracion
     * @return retorna true si son iguales o false si no
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Administrativo)) return false;
        Administrativo tmpAdministrativo = (Administrativo) o;
        return getNombramiento().equals(tmpAdministrativo.getNombramiento())
                && getIdCard().equals(tmpAdministrativo.getIdCard())
                && getNombre().equals(tmpAdministrativo.getNombre())
                && getPrimerApellido().equals(tmpAdministrativo.getPrimerApellido());
    }

    /**
     * Crea un código unico de tipo hash con los atributos del objeto
     *
     * @return el codigo hash generado con algunos de sus atributos
     */
    @Override
    public int hashCode() {
        return Objects.hash(getNombramiento(), getIdCard(), getNombre());
    }

    /**
     * Obtiene el nombramiento del usuairo
     *
     * @return String con el nombramiento del usuario
     */
    public String getNombramiento() {
        return nombramiento;
    }

    /**
     * Cambbiar el valor del nombramiento del usuairo
     *
     * @param nombramiento nuevo valor del nombramiento
     */
    public void setNombramiento(String nombramiento) {
        this.nombramiento = nombramiento;
    }

    /**
     * Obtiene la cantidad de horas por semana que trabaja el usuario administrativo
     *
     * @return numero con las horas por semana que debe trabajar
     */
    public int getHorasSemana() {
        return horasSemana;
    }

    /**
     * Cambia el valor de las horas por semana
     *
     * @param horasSemana nuevo valor de las horas por semana
     */
    public void setHorasSemana(int horasSemana) {
        this.horasSemana = horasSemana;
    }
}
