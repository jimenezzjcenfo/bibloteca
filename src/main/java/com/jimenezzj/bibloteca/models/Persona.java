package com.jimenezzj.bibloteca.models;

/**
 * Represenatcion de una persona dentro de biblo app
 *
 * @author Jonathan Jimenez Zambrana
 * @version 1.0
 */
public class Persona {

    private String idCard;
    private String nombre;
    private String primerApellido;

    /**
     * Crea un objeto de tipo persona
     *
     * @param idCard         identificacoin de una persona
     * @param nombre         nombre de una persona
     * @param primerApellido primer apellido de una persona
     */
    public Persona(String idCard, String nombre, String primerApellido) {
        this.idCard = idCard;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
    }

    /**
     * Crea un objeto de un apersona con los valors sin inicializar
     */
    public Persona() {

    }

    /**
     * Crea un a representacion de tipo String de una perosna
     *
     * @return String que representa una perosna
     */
    @Override
    public String toString() {
        return "Persona [" +
                "idCard=" + idCard +
                ", nombre=" + nombre +
                ", primerApellido=" + primerApellido +
                ']';
    }

    /**
     * Obtiene la identificacion de una persona
     *
     * @return identificaion de una persona
     */
    public String getIdCard() {
        return idCard;
    }

    /**
     * Cambia la identificaicon de una persona
     *
     * @param idCard nuevo valor de la identificacion de una persona
     */
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    /**
     * Obtiene el nombre de una persona
     *
     * @return nombre de la persona
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Cambia el nombre de una persona
     *
     * @param nombre nuevo de nuevo del estado de una persona
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Obtiene el primer apellido de una persona
     *
     * @return primer apellido de la persona
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * Cambia el valor del primer apellido de una persona
     *
     * @param primerApellido nuevo valor para el primer apellido de una persona
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }
}
