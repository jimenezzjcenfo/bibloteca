package com.jimenezzj.bibloteca.models;

import java.util.Objects;

/**
 * Representa un estudiante en biblo app
 *
 * @author Jonathan Jimenez Zambrana
 * @version 1.0
 */
public class Estudiante extends Persona {

    private String carnetId;
    private String carreraCodigo;
    private int credNumActual;

    /**
     * Crea un objeto de tipo estudiante con los atributos especificados
     *
     * @param idCard         identififcaion del estuiante
     * @param nombre         nombre del estudiante
     * @param primerApellido primer apellido del estudiante
     * @param carnetId       carnet del estudiante
     * @param carreraCodigo  el codigo de la carrera que cursa
     * @param credNumActual  la cantidad de creditos de la carrera que cursa
     */
    public Estudiante(String idCard, String nombre, String primerApellido, String carnetId, String carreraCodigo, int credNumActual) {
        super(idCard, nombre, primerApellido);
        this.carnetId = carnetId;
        this.carreraCodigo = carreraCodigo;
        this.credNumActual = credNumActual;
    }

    /**
     * Crea un objeto de tipo estudiante con los valores sin inicializar
     */
    public Estudiante() {

    }

    /**
     * Crea un representacion de tipo String del estudiante
     *
     * @return representacion del estudiante
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "Estudiante [" + getParentString +
                ", carnetId=" + carnetId +
                ", carreraCodigo=" + carreraCodigo +
                ", credNumActual=" + credNumActual +
                ']';
    }

    /**
     * Verifica si este objeto es igual a otro
     *
     * @param o objeto con el que se realiza la compraración
     * @return true si es igual o false si don diferentes
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Estudiante tmpEstudiante = (Estudiante) o;
        return credNumActual == tmpEstudiante.credNumActual &&
                carnetId.equals(tmpEstudiante.carnetId) &&
                Objects.equals(carreraCodigo, tmpEstudiante.carreraCodigo);
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(carnetId, carreraCodigo, credNumActual);
    }

    /**
     * Obtiene la identificaion del estudiante
     *
     * @return identitifacion del estudiante
     */
    public String getCarnetId() {
        return carnetId;
    }

    /**
     * Cambiar el valor de la identificaion del estudiante
     *
     * @param carnetId nuevo valor de la identificacion del estudiante
     */
    public void setCarnetId(String carnetId) {
        this.carnetId = carnetId;
    }

    /**
     * Obtiene el codigo de la carrear del estudiante
     *
     * @return codigo de la carrera
     */
    public String getCarreraCodigo() {
        return carreraCodigo;
    }

    /**
     * Cambiar el valor del codigo de la carreara que cursa el estudiante
     *
     * @param carreraCodigo nuevo valor del codigo de la carrera del estudiante
     */
    public void setCarreraCodigo(String carreraCodigo) {
        this.carreraCodigo = carreraCodigo;
    }

    /**
     * Obtiene el numero de creditos de la carrera del estudiante
     *
     * @return el numero del creditos de la carrera
     */
    public int getCredNumActual() {
        return credNumActual;
    }

    /**
     * Cambia el valor del numero de creditos de la carreara del estudiante
     *
     * @param credNumActual nuevo valor del numero de creditos de la carrera
     */
    public void setCredNumActual(int credNumActual) {
        this.credNumActual = credNumActual;
    }
}
