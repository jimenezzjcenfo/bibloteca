package com.jimenezzj.bibloteca.models;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Representacion de un material comun dentro de biblo app
 *
 * @author Jonthan Jimenez Zambrana
 * @version 1.0
 */
public class Material {

    private String signaturaId;
    private LocalDate fechaCompra;
    private boolean restringido;
    private String tema;

    /**
     * Crea un objeto de tipo material con los atributos especificados
     *
     * @param signaturaId valor unioo del objeto
     * @param fechaCompra fecha de compra del objeto
     * @param restringido estado del material (restringido/no restringido)
     * @param tema        tema del material
     */
    public Material(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema) {
        this.signaturaId = signaturaId;
        this.fechaCompra = fechaCompra;
        this.restringido = restringido;
        this.tema = tema;
    }

    /**
     * Crea un objeto de tipo material con los valores sin inicializar
     */
    public Material() {
    }

    /**
     * Representación de tipo String del Material
     *
     * @return String que representa el Material
     */
    @Override
    public String toString() {
        return "Material [" +
                "signaturaId=" + signaturaId +
                ", fechaCompra=" + fechaCompra +
                ", restringido=" + restringido +
                ", tema='" + tema +
                ']';
    }

    /**
     * Verifica si este objeto es igual a otro
     *
     * @param o objeto con el que va a realizar la compraración
     * @return true si son iguales o false si no lo son
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Material)) return false;
        Material material = (Material) o;
        return getSignaturaId().equals(material.getSignaturaId()) &&
                getFechaCompra().equals(material.getFechaCompra());
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(getSignaturaId(), getFechaCompra());
    }

    /**
     * Ontiene la signatura id del libro
     *
     * @return signatura del objeto
     */
    public String getSignaturaId() {
        return signaturaId;
    }

    /**
     * Cambia la signatura del material
     *
     * @param signaturaId nuevo valor de la signatura del material
     */
    public void setSignaturaId(String signaturaId) {
        this.signaturaId = signaturaId;
    }

    /**
     * Obtiene la fecha de compra del material
     *
     * @return fecha de compra del material
     */
    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    /**
     * Cambia la fecha de compra del material
     *
     * @param fechaCompra nuevo valor para la fecha de compra del material
     */
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * Obtiene el estado del material (restringido/no restringido)
     *
     * @return estado del material
     */
    public boolean isRestringido() {
        return restringido;
    }

    /**
     * Cambia el estado del material
     *
     * @param restringido nuevo valor del estado del material
     */
    public void setRestringido(boolean restringido) {
        this.restringido = restringido;
    }

    /**
     * Obtiene el tema del material
     *
     * @return tema del material
     */
    public String getTema() {
        return tema;
    }

    /**
     * Cambia el tema del material
     *
     * @param tema nuevo valot del material
     */
    public void setTema(String tema) {
        this.tema = tema;
    }
}
