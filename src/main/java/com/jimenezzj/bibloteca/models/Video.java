package com.jimenezzj.bibloteca.models;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

/**
 * Repersentacion de un recurso de video dentro de biblo app
 */
public class Video extends Audio {

    /**
     * Representacion de tipo enumeracion de los posibles formato
     * que puede tener un recurso de video dentro de  biblo app
     */
    public enum EFormatoVideo {
        VHS, VCD, DVD;
    }

    private String nombreDirector;
    private EFormatoVideo videoFormato;
    private int duracionMinutosVideo;

    /**
     * Crea un objeto de tipo Video con los valores especificados
     *
     * @param signaturaId          asgina la signatura id del video
     * @param fechaCompra          asgina fecha de compra del video
     * @param restringido          asgina estado del video
     * @param tema                 asgina tema del video
     * @param formato              asgina formato de audio del video
     * @param duracionMinutos      asgina duracion en minutos del video
     * @param idioma               asgina idioma del video
     * @param nombreDirector       asgina nombre del director del video
     * @param videoFormato         asgina formato del video
     * @param duracionMinutosVideo asgina duracion en minutos del video
     */
    public Video(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, EFormato formato, int duracionMinutos, String idioma,
                 String nombreDirector, EFormatoVideo videoFormato, int duracionMinutosVideo) {
        super(signaturaId, fechaCompra, restringido, tema, formato, duracionMinutos, idioma);
        this.nombreDirector = nombreDirector;
        this.videoFormato = videoFormato;
        this.duracionMinutosVideo = duracionMinutosVideo;
    }

    /**
     * Crea un objeto de tipo Video con los valores sin inicializar
     */
    public Video() {
    }

    /**
     * Crea una representacion de tipo String de Video
     *
     * @return String que representa el video
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "Video [" + getParentString +
                "nombreDirector='" + nombreDirector +
                ", videoFormato=" + videoFormato +
                ", duracionMinutosVideo=" + duracionMinutosVideo +
                ']';
    }

    /**
     * Verifica si este objeto es igual a otro
     *
     * @param o objeto con el que va a realizar la compraración
     * @return true si son iguales o false si no lo son
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Video)) return false;
        if (!super.equals(o)) return false;
        Video video = (Video) o;
        return getDuracionMinutosVideo() == video.getDuracionMinutosVideo() &&
                getNombreDirector().equals(video.getNombreDirector()) &&
                getVideoFormato() == video.getVideoFormato()
                && getSignaturaId().equals(video.getSignaturaId())
                && getFechaCompra().isEqual(video.getFechaCompra());
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNombreDirector(), getVideoFormato(), getDuracionMinutosVideo());
    }

    /**
     * Obtiene el nombre delk director del video
     *
     * @return nombre del directot
     */
    public String getNombreDirector() {
        return nombreDirector;
    }

    /**
     * Ajusta el nombre del director del video
     *
     * @param nombreDirector nuevo nombre del director del video
     */
    public void setNombreDirector(String nombreDirector) {
        this.nombreDirector = nombreDirector;
    }

    /**
     * Obtiene el formato de video del objeto de tipo Video
     *
     * @return el formato del video
     */
    public EFormatoVideo getVideoFormato() {
        return videoFormato;
    }

    /**
     * Aujusta el formato de video del objeto tipo Video
     *
     * @param videoFormato nuevo valor del formato del video
     */
    public void setVideoFormato(EFormatoVideo videoFormato) {
        this.videoFormato = videoFormato;
    }

    /**
     * Obtiene la duracion en minutos del video
     *
     * @return duracion en minutos del video
     */
    public int getDuracionMinutosVideo() {
        return duracionMinutosVideo;
    }

    /**
     * Ajusta el valor de la duracion en minutos del video
     *
     * @param duracionMinutosVideo nuevo valor de la duracion en minutos del video
     */
    public void setDuracionMinutosVideo(int duracionMinutosVideo) {
        this.duracionMinutosVideo = duracionMinutosVideo;
    }
}
