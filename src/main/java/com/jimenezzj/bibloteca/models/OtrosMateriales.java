package com.jimenezzj.bibloteca.models;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Representacion de otro material en la biblo app
 *
 * @author Jonathan Jimenez Zambrana
 * @version 1.0
 */
public class OtrosMateriales extends Material {

    private String descripcion;

    /**
     * Crea un objeto de tipo otro material
     *
     * @param signaturaId signatura id de otro material
     * @param fechaCompra fecha de compra de otro material
     * @param restringido estado de otro material
     * @param tema        tema de otro material
     * @param descripcion descripcion de otro material
     */
    public OtrosMateriales(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, String descripcion) {
        super(signaturaId, fechaCompra, restringido, tema);
        this.descripcion = descripcion;
    }

    /**
     * Crea un objeto de tipo otro material con lo valores sin inicializar
     */
    public OtrosMateriales() {
    }

    /**
     * Crea una representacion de tipo String de otro objeto
     *
     * @return String que representa un objeto
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "OtroMaterial [" + getParentString +
                "descripcion=" + descripcion +
                ']';
    }

    /**
     * Verifica si este objeto es igual a otro
     *
     * @param o objeto con el que va a realizar la compraración
     * @return true si son iguales o false si no lo son
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtrosMateriales)) return false;
        if (!super.equals(o)) return false;
        OtrosMateriales tmpOtro = (OtrosMateriales) o;
        return getDescripcion().equals(tmpOtro.getDescripcion())
                && getFechaCompra().isEqual(tmpOtro.getFechaCompra())
                && getSignaturaId().equals(tmpOtro.getSignaturaId());
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDescripcion());
    }

    /**
     * Otiene la decripcion de otro material
     *
     * @return descripcion de otro material
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Cambia el valor de descripcion de otro material
     *
     * @param descripcion nuevo valor para la descripcion de otro materiak
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
