package com.jimenezzj.bibloteca.models;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Crea un representacion de un texto dentro del biblo app
 */
public class Textos extends Material {
    private String titulo;
    private String nombreAutor;
    private LocalDate fechaPublic;
    private int pagesNum;
    private String idioma;

    /**
     * Crea un objeto de tipo texto con los atributos especificados
     *
     * @param signaturaId asigna la signatura id de un texto
     * @param fechaCompra asigna la fedha de compra del texto
     * @param restringido asigna el estado del texto
     * @param tema        asigna el tema del texto
     * @param titulo      asigna el titulo del texto
     * @param nombreAutor asigna nombre del autor del texto
     * @param fechaPublic asigna la fecha de publicacion del texto
     * @param pagesNum    numero de paginas del texto
     * @param idioma      asigna el idioma del texto
     */
    public Textos(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, String titulo, String nombreAutor,
                  LocalDate fechaPublic, int pagesNum, String idioma) {
        super(signaturaId, fechaCompra, restringido, tema);
        this.titulo = titulo;
        this.nombreAutor = nombreAutor;
        this.fechaPublic = fechaPublic;
        this.pagesNum = pagesNum;
        this.idioma = idioma;
    }

    /**
     * Crea un objeto de tipo texto con los atributos sin inicialiar
     */
    public Textos() {
    }

    /**
     * Crea una representaicon de tipo String del texto
     *
     * @return representacion String del Texto
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "Textos [" + getParentString +
                "titulo=" + titulo +
                ", nombreAutor=" + nombreAutor +
                ", fechaPublic=" + fechaPublic +
                ", pagesNum=" + pagesNum +
                ", idioma=" + idioma +
                ']';
    }

    /**
     * Verifica si este objeto es igual a otro
     *
     * @param o objeto con el que va a realizar la compraración
     * @return true si son iguales o false si no lo son
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Textos)) return false;
        if (!super.equals(o)) return false;
        Textos textos = (Textos) o;
        return getPagesNum() == textos.getPagesNum() &&
                getTitulo().equals(textos.getTitulo()) &&
                getNombreAutor().equals(textos.getNombreAutor()) &&
                getFechaPublic().equals(textos.getFechaPublic());
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTitulo(), getNombreAutor(), getFechaPublic(), getSignaturaId());
    }

    /**
     * Ontiene el titulo del texto
     *
     * @return titulo del texto
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Cambia el valot del titulo del texto
     *
     * @param titulo nuevo valor par ael titulo del texto
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }


    /**
     * Obtiene el nombre del autor del texto
     *
     * @return valor del nombre del autor del texto
     */
    public String getNombreAutor() {
        return nombreAutor;
    }

    /**
     * Cambia el nombre del autor del texto
     *
     * @param nombreAutor nuevo nombre del autor del texto
     */
    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    /**
     * Obtiene el valor de la fecha de publicacion del texto
     *
     * @return fecha de publicacion del texto
     */
    public LocalDate getFechaPublic() {
        return fechaPublic;
    }

    /**
     * Cambia el valot de la fecha de publicacion del texto
     *
     * @param fechaPublic nueva fecha de publicacion del texto
     */
    public void setFechaPublic(LocalDate fechaPublic) {
        this.fechaPublic = fechaPublic;
    }

    /**
     * Obtiene el numero de paginas del texto
     *
     * @return numero de paginas del texto
     */
    public int getPagesNum() {
        return pagesNum;
    }

    /**
     * Ajusta el valor del numnero de paginas del texto
     *
     * @param pagesNum nuevo numero de paginas del texto
     */
    public void setPagesNum(int pagesNum) {
        this.pagesNum = pagesNum;
    }

    /**
     * Obtiene el idioma del texto
     *
     * @return el idiona del texto
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * Ajusta el valor del idioma del texto
     *
     * @param idioma nuevo valor del idioma del texto
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
}
