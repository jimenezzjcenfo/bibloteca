package com.jimenezzj.bibloteca.models;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Representa a un profesor dentro biblo app
 */
public class Profesor extends Persona {

    private String tipoContrato;
    private LocalDate fechaContratacion;

    /**
     * Crea un objeto de tipo Profesor con lo valor indicados
     *
     * @param idCard            identificaion del profesor
     * @param nombre            nombre del profesor
     * @param primerApellido    primer apellido del profesor
     * @param tipoContrato      tipo de contrato del profesor
     * @param fechaContratacion fecha de contratacion del profesor
     */
    public Profesor(String idCard, String nombre, String primerApellido, String tipoContrato,
                    LocalDate fechaContratacion) {
        super(idCard, nombre, primerApellido);
        this.tipoContrato = tipoContrato;
        this.fechaContratacion = fechaContratacion;
    }

    /**
     * Crea un objeto de tipo profesor con lo valores sin inicializar
     */
    public Profesor() {
    }

    /**
     * Crea un arepresentacion de tipo String de un profesor
     *
     * @return String que representa un profesor
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "Profesor [" + getParentString +
                "tipoContrato=" + tipoContrato +
                ", fechaContratacion=" + fechaContratacion.toString() +
                ']';
    }

    /**
     * Verifica si este objeto es igual a otro
     *
     * @param o objeto con el que va a realizar la compraración
     * @return true si son iguales o false si no lo son
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Profesor)) return false;
        if (!super.equals(o)) return false;
        Profesor tmpProfesor = (Profesor) o;
        return getFechaContratacion().equals(tmpProfesor.getFechaContratacion())
                && getIdCard().equals(tmpProfesor.getIdCard()) && getTipoContrato().equals(tmpProfesor.getTipoContrato());
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFechaContratacion());
    }

    /**
     * Obtiene el tipo del contrato del profesor
     *
     * @return tipo de contrato del profesor
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Cambia el valot del tipo de contrato del profesor
     *
     * @param tipoContrato nuevo valot del tipo de contrato del profesor
     */
    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    /**
     * Obtiene la fecha de la contratacion del profesor
     * @return fecha de contratacion del profesor
     */
    public LocalDate getFechaContratacion() {
        return fechaContratacion;
    }

    /**
     * Cambia la fechja de conrtatacion del profesor
     * @param fechaContratacion nuevo valor para la fecha de contratacion del profesor
     */
    public void setFechaContratacion(LocalDate fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }
}
