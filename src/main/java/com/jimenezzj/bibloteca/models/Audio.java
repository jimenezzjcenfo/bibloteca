package com.jimenezzj.bibloteca.models;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Es la representacion de un material de tipo audio el biblo app
 *
 * @author Jonathan Jiménez Zambrana
 * @version 1.0
 */
public class Audio extends Material {

    /**
     * Enumeracion con las opciones de formato de audio que puede
     * ser el audio
     */
    public enum EFormato {
        CASETE, CD, DV;
    }

    private EFormato formato;
    private int duracionMinutos;
    private String idioma;

    /**
     * Crea un objeto de tipo audio
     *
     * @param signaturaId     nuevo valor con identificaion unica del audio
     * @param fechaCompra     nuevo valor con la fecha de compra del audio
     * @param restringido     nuevo valor del estado del audio
     * @param tema            nuevo tema del audio
     * @param formato         nuevo valor con el formato del audio
     * @param duracionMinutos valor con la duracion en minutos del audio
     * @param idioma          valor del idioma del audio
     */
    public Audio(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, EFormato formato, int duracionMinutos, String idioma) {
        super(signaturaId, fechaCompra, restringido, tema);
        this.formato = formato;
        this.duracionMinutos = duracionMinutos;
        this.idioma = idioma;
    }

    /**
     * Crea un objeto de audio con los valor sin inicializar
     */
    public Audio() {
    }

    /**
     * Representacion de tipo String de un objeto de tipo audio
     *
     * @return representacion tipo String del audio
     */
    @Override
    public String toString() {
        String getParentString = super.toString().split("\\[|]")[1].concat(", ");
        return "Audio [" + getParentString +
                "formato=" + formato +
                ", duracionMinutos=" + duracionMinutos +
                ", idioma='" + idioma +
                ']';
    }

    /**
     * Realiza un comparacion entre dos objetos para verificar si so iguales
     *
     * @param o objeto con el que se realizar  la compraracion
     * @return resultado de tipo boolean de la verificacion
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Audio)) return false;
        if (!super.equals(o)) return false;
        Audio audio = (Audio) o;
        return getDuracionMinutos() == audio.getDuracionMinutos() &&
                getFormato() == audio.getFormato()
                && getSignaturaId().equals(audio.getSignaturaId())
                && getFechaCompra().isEqual(audio.getFechaCompra());
    }

    /**
     * Crea un hash unico con algunos de los atributos del objeto
     *
     * @return codigo hash unico gnerado
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFormato(), getDuracionMinutos(), getSignaturaId(),
                getFechaCompra());
    }

    /**
     * Obtiene el formato del audio. Ver {@link com.jimenezzj.bibloteca.models.Audio.EFormato}
     *
     * @return El formato del audio
     */
    public EFormato getFormato() {
        return formato;
    }

    /**
     * Cambia el valor del formato del auido
     *
     * @param formato nuevo valor para el de formato para el audio
     */
    public void setFormato(EFormato formato) {
        this.formato = formato;
    }

    /**
     * Obtiene la duracion en minutos del audio
     *
     * @return duracion en minutos del audio
     */
    public int getDuracionMinutos() {
        return duracionMinutos;
    }

    /**
     * Cambia el valor de la duracion en minutos del audio
     *
     * @param duracionMinutos nuevo valor de la duracion en minutos de l audio
     */
    public void setDuracionMinutos(int duracionMinutos) {
        this.duracionMinutos = duracionMinutos;
    }

    /**
     * Obtiene el idioma del audio
     *
     * @return idioma del audio
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * Cambia el valor del idioma del audio
     *
     * @param idioma nuevo valor del idioma del audio
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
}
