package com.jimenezzj.bibloteca.view_console;

import java.io.BufferedReader;
import java.io.IOException;

public class MenuView {

    private BufferedReader reader;

    private PersonaView personaView;
    private MaterialView materialView;

    public  MenuView(BufferedReader reader) {
        this.reader = reader;
        this.personaView = new PersonaView(reader);
        this.materialView = new MaterialView(reader);
    }

    public  void  start() throws IOException {
        showMenu();
    }

    private void showMenu() throws  IOException{
        int op = 0;
        String menu = "- - - - - - - - -\n" + "M E N U\n" + "- - - - - - - - -\n";
        menu += "1) Personas\n" + "2) Materiales\n" + "3) Salir";
        while (op != 3) {
            System.out.println(menu);
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    personaView.start();
//				setMotorView(null);
                    break;
                case 2:
                    materialView.start();
//				setVehicleView(null);
                    break;
                case 3:
                    op = 3;
                    System.out.println("Has salido de Bibloteca app");
                    break;
                default:
                    System.out.println("Opcion invalida!");
                    break;
            }
        }
    }
}
