package com.jimenezzj.bibloteca.view_console;

import com.jimenezzj.bibloteca.controllers.UsuarioController;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDate;

public class PersonaView {

    private BufferedReader reader;

    private UsuarioController userController;

    public PersonaView(BufferedReader pReader) {
        this.reader = pReader;
        this.userController = new UsuarioController();
    }

    public void start() throws IOException {
        showMenu();
    }

    public void showMenu() throws IOException {
        int op = 0;
        String menu = "- - - - - - - - -\n" + "M E N U\n" + "menu -> persona\n" + "- - - - - - - - -\n";
        menu += "1) Ver Listas de Personas\n" + "2) Añadir administrativo\n" + "3) Añadir profesor\n" + "4) Añadir estudiante\n" + "5) Regresar";
        while (op != 5) {
            System.out.println(menu);
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    showListasUsuarios();
                    break;
                case 2:
                    showAddAdministrativo();
                    break;
                case 3:
                    showAddProfesor();
                    break;
                case 4:
                    showAddEstudiante();
                    break;
                case 5:
                    op = 5;
                    System.out.println("Regresando...");
                    break;
                default:
                    System.out.println("Opcion invalida!");
                    break;
            }
        }
    }

    private void showListasUsuarios() throws IOException {
        String header = "- - - - - - - >" + "Listas de Usuarios" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("Selecciona la lista del tipo de usuarios que desea ver\n"
                + "1)Administrativo, 2)Profesor, 3)Estudiante, 5) TODOS");
        int opList = Integer.parseInt(reader.readLine());
        String tipoUsuario = null;

        if (opList == 1) tipoUsuario = "Administrativo";
        if (opList == 2) tipoUsuario = "Profesor";
        if (opList == 3) tipoUsuario = "Estudiante";

        try {
            System.out.println("- - - - - - - " + (tipoUsuario == null ? "Personas" : tipoUsuario) + " - - - - - - - - - \n");
            userController.getAllUsers(tipoUsuario).forEach(System.out::println);
            System.out.println("\n- - - - - - - " + (tipoUsuario == null ? "Personas" : tipoUsuario) + " - - - - - - - - - ");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "No se pudo obtener la lista!");
            showMenu();
        }
    }

    private void showAddAdministrativo() throws IOException {
        String header = "- - - - - - - >" + "Agregar Usuario Administrativo" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese su identificacion");
        String idCard = reader.readLine();
        System.out.println("> Ingrese el nombre");
        String nombre = reader.readLine();
        System.out.println("> Ingrese el primer apellido");
        String primerApellido = reader.readLine();
        System.out.println("> Seleccion el tipo de nombramiento\n" + "1)A, " + "2 )B, " + "3)C");
        int opNombramiento = Integer.parseInt(reader.readLine());
        String nombramiento = "NINGUNO";
        if (opNombramiento > 3 || opNombramiento < 1) {
            System.out.println("Opcion invalida");
            showAddAdministrativo();
        }
        if (opNombramiento == 1) nombramiento = "A";
        if (opNombramiento == 2) nombramiento = "B";
        if (opNombramiento == 3) nombramiento = "C";
        System.out.println("> Ingrese el numero de horas por semana");
        int numHorasSemana = Integer.parseInt(reader.readLine());
        try {
            userController.addAdministrativo(idCard, nombre, primerApellido, nombramiento, numHorasSemana);
            System.out.println("> Se agrego el administrativo con exito!");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentalo nuevamente");
            showMenu();
        }
    }

    private void showAddEstudiante() throws IOException {
        String header = "- - - - - - - >" + "Agregar Usuario Estudiante" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese su identificacion");
        String idCard = reader.readLine();
        System.out.println("> Ingrese su numero de carnet");
        String numCarnet = reader.readLine();
        System.out.println("> Ingrese el nombre");
        String nombre = reader.readLine();
        System.out.println("> Ingrese el primer apellido");
        String primerApellido = reader.readLine();
        System.out.println("> Ingrese el código de la carrera");
        String codCarrera = reader.readLine();
        System.out.println("> Ingrese el numero de creaditos de la carrera");
        int numCreditos = Integer.parseInt(reader.readLine());
        try {
            userController.addEstudiante(idCard, nombre, primerApellido, numCarnet, codCarrera, numCreditos);
            System.out.println("> Se agrego el estudiante con exito!");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentalo nuevamente");
            showMenu();
        }
    }

    private void showAddProfesor() throws IOException {
        String header = "- - - - - - - >" + "Agregar Usuario Profesor" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese su identificacion");
        String idCard = reader.readLine();
        System.out.println("> Ingrese el nombre");
        String nombre = reader.readLine();
        System.out.println("> Ingrese le primer apellido");
        String primerApellido = reader.readLine();
        System.out.println("> Ingrese la fecha de contratación\n" + "Formato: dd/mm/yyyy");
        String[] inDate = reader.readLine().split("/");
        LocalDate date = LocalDate.of(Integer.parseInt(inDate[2]),
                Integer.parseInt(inDate[1]) , Integer.parseInt(inDate[0]));
        System.out.println("> Seleccione el tipo de contrato\n" + "1) Tiempo Completo, 2) Medio tiempo");
        int opContrato = Integer.parseInt(reader.readLine());
        String tipoContrato = "NINGUNO";
        if (opContrato == 1) tipoContrato = "COMPLETO";
        if (opContrato == 2) tipoContrato = "MEDIO";
        if (opContrato > 2 || opContrato < 1) {
            System.out.println("OPcion Invalida\n" + "Intentalo nuevamente");
            showMenu();
        }

        try {
            userController.addProfesor(idCard, nombre, primerApellido, tipoContrato, date);
            System.out.println("> Se agrego el Profesor con exito!");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentalo nuevamente");
            showMenu();
        }
    }


}
