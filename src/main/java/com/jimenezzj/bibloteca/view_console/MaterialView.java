package com.jimenezzj.bibloteca.view_console;

import com.jimenezzj.bibloteca.controllers.MaterialController;

import java.io.BufferedReader;
import java.io.IOException;
import java.time.LocalDate;

public class MaterialView {
    private final BufferedReader reader;

    private final MaterialController materialController;

    public MaterialView(BufferedReader reader) {
        this.reader = reader;
        this.materialController = new MaterialController();
    }

    public void start() throws IOException {
        showMenu();
    }

    public void showMenu() throws IOException {
        int op = 0;
        String menu = "- - - - - - - - -\n" + "M E N U\n" + "menu -> material\n" + "- - - - - - - - -\n";
        menu += "1) Ver listas de materiales\n" + "2) Añadir Audio\n" + "3) Añadir Video\n" + "4) Añadir Texto\n" + "5) Añadir otro\n" + "6) Regresar";
        while (op != 6) {
            System.out.println(menu);
            op = Integer.parseInt(reader.readLine());
            switch (op) {
                case 1:
                    showListas();
                    break;
                case 2:
                    showAddAudio();
                    break;
                case 3:
                    showAddVideo();
                    break;
                case 4:
                    showAddTexto();
                    break;
                case 5:
                    showAddOtro();
                    break;
                case 6:
                    op = 6;
                    System.out.println("Regresando...");
                    break;
                default:
                    System.out.println("Opcion invalida!");
                    break;
            }
        }
    }

    private void showListas() throws IOException {
        String header = "- - - - - - - >" + "Listas de Materiales" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("Selecciona la lista del tipo de materiales que desea ver\n"
                + "1)Audio, 2)Video, 3)Texto, 4)Otros 5) TODOS");
        int opList = Integer.parseInt(reader.readLine());
        String tipoMaterial = null;
        if (opList == 1) tipoMaterial = "Audio";
        if (opList == 2) tipoMaterial = "Video";
        if (opList == 3) tipoMaterial = "Textos";
        if (opList == 4) tipoMaterial = "Otro";

        try {
            System.out.println("- - - - - - - " + (tipoMaterial == null ? "Material" : tipoMaterial) + " - - - - - - - - - \n");
            materialController.getAllMaterials(tipoMaterial).forEach(System.out::println);
            System.out.println("\n- - - - - - - " + (tipoMaterial == null ? "Material" : tipoMaterial) + " - - - - - - - - - ");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "No se pudo obtener la lista!");
            showMenu();
        }

    }

    private void showAddAudio() throws IOException {
        String header = "- - - - - - - >" + "Agregar Audio" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese la signaruta id del audio");
        String signaturaId = reader.readLine();

        System.out.println("> Ingrese la fecha de compra del libro\n"+"Formato: d/m/yyyy");
        String[] inDate = reader.readLine().split("/");
        LocalDate date = LocalDate.of(Integer.parseInt(inDate[2]),
                Integer.parseInt(inDate[1]) , Integer.parseInt(inDate[0]) );

        System.out.println("> Seleccione el estado del audio\n" + "1) Restringido, 2)No restringido");
        int opRes = Integer.parseInt(reader.readLine());
        boolean restrinccion = false;
        if (opRes == 1) restrinccion = true;

        System.out.println("> Ingrese el tema del audio");
        String temaAudio = reader.readLine();

        System.out.println("> Seleccione el formato del audio\n" + "1) Casete, 2)CD, 3) DVD");
        int opTipoAudio = Integer.parseInt(reader.readLine());
        String tipoAudio = "SINFORMATO";
        if (opTipoAudio == 1) tipoAudio = "CASETE";
        if (opTipoAudio == 2) tipoAudio = "CD";
        if (opTipoAudio == 3) tipoAudio = "DVD";

        System.out.println("> Ingrese la durtación en minutos del audio");
        int audioDurMin = Integer.parseInt(reader.readLine());

        System.out.println("> Ingrese el idioma del audio");
        String idioma = reader.readLine();

        try {
            materialController.addAudio(signaturaId, date, restrinccion, temaAudio, tipoAudio, audioDurMin, idioma);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentelo nuevamente");
            showMenu();
        }
    }

    private void showAddVideo() throws IOException {
        String header = "- - - - - - - >" + "Agregar Video" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese la signaruta id del video");
        String signaturaId = reader.readLine();

        System.out.println("> Ingrese la fecha de compra del video");
        String[] inDate = reader.readLine().split("/");
        LocalDate date = LocalDate.of(Integer.parseInt(inDate[2]),
                Integer.parseInt(inDate[1]) - 1, Integer.parseInt(inDate[0]) - 1);

        System.out.println("> Seleccione el estado del video\n" + "1) Restringido, 2)No restringido");
        int opRes = Integer.parseInt(reader.readLine());
        boolean restrinccion = false;
        if (opRes == 1) restrinccion = true;

        System.out.println("> Ingrese el tema del audio");
        String temaAudio = reader.readLine();

        System.out.println("> Seleccione el formato del audio\n" + "1) Casete, 2)CD, 3) DVD");
        int opTipoAudio = Integer.parseInt(reader.readLine());
        String tipoAudio = "SINFORMATO";
        if (opTipoAudio == 1) tipoAudio = "CASETE";
        if (opTipoAudio == 2) tipoAudio = "CD";
        if (opTipoAudio == 3) tipoAudio = "DVD";

        System.out.println("> Ingrese la durtación en minutos del audio");
        int audioDurMin = Integer.parseInt(reader.readLine());

        System.out.println("> Ingrese el idioma del audio");
        String idioma = reader.readLine();

        System.out.println("> Ingrese le director del video");
        String directorNombre = reader.readLine();

        System.out.println("> Ingrese la duración en minutos del video");
        int duracionVideo = Integer.parseInt(reader.readLine());

        System.out.println("> Seleccione el formato del video\n" + "1) VHS, 2)VCD, 3)DVD");
        int opTipoVideo = Integer.parseInt(reader.readLine());
        String tipoVideo = "SINFORMATO";
        if (opTipoVideo == 1) tipoVideo = "VHS";
        if (opTipoVideo == 2) tipoVideo = "VCD";
        if (opTipoVideo == 3) tipoVideo = "DVD";

        try {
            materialController.addVideo(signaturaId, date, restrinccion, temaAudio, tipoAudio, audioDurMin, idioma,
                    directorNombre, tipoVideo, duracionVideo);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentelo nuevamente");
            showMenu();
        }
    }

    private void showAddTexto() throws IOException {
        String header = "- - - - - - - >" + "Agregar Texto" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese la signaruta id del texto");
        String signaturaId = reader.readLine();

        System.out.println("> Ingrese la fecha de compra del libro");
        String[] inDate = reader.readLine().split("/");
        LocalDate date = LocalDate.of(Integer.parseInt(inDate[2]),
                Integer.parseInt(inDate[1]) - 1, Integer.parseInt(inDate[0]) - 1);

        System.out.println("> Seleccione el estado del texto\n" + "1) Restringido, 2)No restringido");
        int opRes = Integer.parseInt(reader.readLine());
        boolean restrinccion = false;
        if (opRes == 1) restrinccion = true;

        System.out.println("> Ingrese el tema del texto");
        String temaAudio = reader.readLine();

        System.out.println("> Ingrese el titulo del texto");
        String tituloTexto = reader.readLine();


        System.out.println("> Ingrese el nombre del autor");
        String nombreAutor = reader.readLine();

        System.out.println("> Ingrese la fecha de publidacion del libro");
        String[] inDatePublic = reader.readLine().split("/");
        LocalDate datePublic = LocalDate.of(Integer.parseInt(inDate[2]),
                Integer.parseInt(inDate[1]) - 1, Integer.parseInt(inDate[0]) - 1);

        System.out.println("> Ingrese el numero de paginas del texto");
        int numeroPag = Integer.parseInt(reader.readLine());

        System.out.println("> Ingrese el idioma del texto");
        String idiomaText = reader.readLine();

        try {
            materialController.addTextos(signaturaId, date, restrinccion, temaAudio,
                    tituloTexto, nombreAutor, datePublic, numeroPag, idiomaText);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentelo nuevamente");
            showMenu();
        }
    }

    private void showAddOtro() throws IOException {
        String header = "- - - - - - - >" + "Agregar Texto" + "<- - - - - - -\n";
        System.out.println(header);
        System.out.println("> Ingrese la signaruta id del material");
        String signaturaId = reader.readLine();

        System.out.println("> Ingrese la fecha de compra del material");
        String[] inDate = reader.readLine().split("/");
        LocalDate date = LocalDate.of(Integer.parseInt(inDate[2]),
                Integer.parseInt(inDate[1]) - 1, Integer.parseInt(inDate[0]) - 1);

        System.out.println("> Seleccione el estado del material\n" + "1) Restringido, 2)No restringido");
        int opRes = Integer.parseInt(reader.readLine());
        boolean restrinccion = false;
        if (opRes == 1) restrinccion = true;

        System.out.println("> Ingrese el tema del material");
        String temaMaterial = reader.readLine();

        System.out.println("> Ingrese una descripcion del material");
        String descripcion = reader.readLine();

        try {
            materialController.addOtroMaterial(signaturaId, date, restrinccion, temaMaterial, descripcion);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\n" + "Intentelo nuevamente");
            showMenu();
        }

    }


}
