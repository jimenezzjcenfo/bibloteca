package com.jimenezzj.bibloteca;

import com.jimenezzj.bibloteca.view_console.MenuView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class App {
    public static MenuView menuView = new MenuView(new BufferedReader(new InputStreamReader(System.in)));

    public static void main(String[] args) throws IOException {
        menuView.start();
    }
}
