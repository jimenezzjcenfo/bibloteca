package com.jimenezzj.bibloteca.dao;

import com.jimenezzj.bibloteca.models.*;
import com.jimenezzj.bibloteca.util.Directories;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Representa un objeto de acceso para los datos persistentes relacionados con
 * los materiales dentro de biblo app
 */
public class MaterialDAO {

    private final String MATERIALES_ARCHIVO = "materiales.txt";

    /**
     * Crea un objeto de material de acceso a datos
     */
    public MaterialDAO() {
    }

    /**
     * Guarda la representacion de un Sting material ({@link com.jimenezzj.bibloteca.models.Material})
     * en un archivo de texto persistente
     *
     * @param nuevoMaterial nuevo objeto de tipo Material
     * @throws IOException Excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addMaterial(Material nuevoMaterial) throws IOException {
        Directories.writeInFile(MATERIALES_ARCHIVO, nuevoMaterial.toString());
    }

    /**
     * Devuelve una lista de tipo Material()
     * con todos los materiales o del tipo que se especifique
     * que se encuentran en el arhivo de texto
     *
     * @param typeMaterial tipo de material que retornara la lista
     * @return una lista de tipo ({@link com.jimenezzj.bibloteca.models.Material})
     * @throws IOException Excepcion que ocurre cuando se escribe un dato erroneamente
     */
    public List<Material> fetchMateriles(String typeMaterial) throws IOException {
        Path tmpPath = Paths.get(Directories.getFilesDir().concat(MATERIALES_ARCHIVO));
        return Files.lines(tmpPath)
                .filter(line -> {
                    if (typeMaterial != null)
                        return line.toLowerCase().contains(typeMaterial.toLowerCase());
                    return !line.isBlank();
                })
                .map(this::transformArrayToMaterialType).collect(Collectors.toList());
    }

    /**
     * Crea un lista de objetos con el tipo de Material indicado.
     * Si es null retorna materiales de todos los tipos
     *
     * @param typeMaterial tipo de objeto del que va ser la lista
     * @return
     */
    private Material transformArrayToMaterialType(String typeMaterial) {
        List<String> datos;
        if (typeMaterial.contains("Audio")) {
            datos = Directories.getDataFromString(typeMaterial, "Audio");
            return new Audio(datos.get(0), LocalDate.parse(datos.get(1)), Boolean.parseBoolean(datos.get(2)), datos.get(3),
                    Audio.EFormato.valueOf(datos.get(4)), Integer.parseInt(datos.get(5)), datos.get(6));
        }
        if (typeMaterial.contains("Video")) {
            datos = Directories.getDataFromString(typeMaterial, "Video");
            return new Video(datos.get(0), LocalDate.parse(datos.get(1)), Boolean.parseBoolean(datos.get(2)), datos.get(3),
                    Audio.EFormato.valueOf(datos.get(4)), Integer.parseInt(datos.get(5)), datos.get(6), datos.get(7),
                    Video.EFormatoVideo.valueOf(datos.get(8)), Integer.parseInt(datos.get(9)));
        }
        if (typeMaterial.contains("Textos")) {
            datos = Directories.getDataFromString(typeMaterial, "Textos");
            return new Textos(datos.get(0), LocalDate.parse(datos.get(1)), Boolean.parseBoolean(datos.get(2)), datos.get(3),
                    datos.get(4), datos.get(5), LocalDate.parse(datos.get(6)), Integer.parseInt(datos.get(7)), datos.get(8));
        }
        if (typeMaterial.contains("Otro")) {
            datos = Directories.getDataFromString(typeMaterial, "OtroMaterial");
            return new OtrosMateriales(datos.get(0), LocalDate.parse(datos.get(1)), Boolean.parseBoolean(datos.get(2)), datos.get(3),
                    datos.get(4));
        }
        datos = Directories.getDataFromString(typeMaterial, "Video");
        return new Material();
    }

}
