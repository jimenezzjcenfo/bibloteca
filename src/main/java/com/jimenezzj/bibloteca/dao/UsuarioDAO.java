package com.jimenezzj.bibloteca.dao;

import com.jimenezzj.bibloteca.models.Administrativo;
import com.jimenezzj.bibloteca.models.Estudiante;
import com.jimenezzj.bibloteca.models.Persona;
import com.jimenezzj.bibloteca.models.Profesor;
import com.jimenezzj.bibloteca.util.Directories;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Representa un objeto de acceso para los datos persistentes relacionados con
 * los usuarios dentro de biblo app
 */
public class UsuarioDAO {

    private final String PERSONAS_ARCHIVO = "usuarios.txt";

    /**
     * Crea un objeto de tipo usuario de acceso a datos
     */
    public UsuarioDAO() {
    }

    /**
     * Guardo un objeto en un archivo de texto para percistir los datos
     *
     * @param nuevaPersona nuevo objeto de tipo persona
     * @throws IOException Excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addUsuario(Persona nuevaPersona) throws IOException {
        Directories.writeInFile(PERSONAS_ARCHIVO, nuevaPersona.toString());
    }

    /**
     * Devuelve una lista de objetos de tipo Persona que puede ser de todos
     * los tipos o del que se especifique. Ver{@link com.jimenezzj.bibloteca.models.Persona}
     *
     * @param typeUser valor del tipo de Persona que contendra la lista
     * @return lista de objetos de tipo Persona
     * @throws IOException excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public List<Persona> fetchPersonas(String typeUser) throws IOException {
        var p = Paths.get(Directories.getFilesDir().concat(PERSONAS_ARCHIVO));
        return Files.lines(p)
                .filter(line -> {
                    if (typeUser != null) return line.toLowerCase().contains(typeUser.toLowerCase());
                    return !line.isBlank();
                })
                .map(this::transforStringtoPersonaTipo).collect(Collectors.toList());
    }

    /**
     * Crea un String en un objeto de Persona o el que se especifique
     *
     * @param personaString valor de los objetos que contendra ls lista
     * @return lista de objetos de tipo persona
     */
    private Persona transforStringtoPersonaTipo(String personaString) {
        if (personaString.contains("Administrativo")) {
            List<String> datos = Directories.getDataFromString(personaString, "Administrativo");
            return new Administrativo(datos.get(0), datos.get(1), datos.get(2),
                    datos.get(3), Integer.parseInt(datos.get(4)));
        }
        if (personaString.contains("Estudiante")) {
            List<String> datos = Directories.getDataFromString(personaString, "Estudiante");
            return new Estudiante(datos.get(0), datos.get(1), datos.get(2),
                    datos.get(3), datos.get(4), Integer.parseInt(datos.get(5)));
        }
        if (personaString.contains("Profesor")) {
            List<String> datos = Directories.getDataFromString(personaString, "Profesor");
            return new Profesor(datos.get(0), datos.get(1), datos.get(2),
                    datos.get(3), LocalDate.parse(datos.get(4)));
        }
        List<String> datos = Directories.getDataFromString(personaString, "Persona");
        return new Persona(datos.get(0), datos.get(1), datos.get(2));
    }

}
