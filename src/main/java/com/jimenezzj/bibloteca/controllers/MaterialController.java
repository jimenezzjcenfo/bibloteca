package com.jimenezzj.bibloteca.controllers;

import com.jimenezzj.bibloteca.dao.MaterialDAO;
import com.jimenezzj.bibloteca.models.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Representación de un objeto que maneja las acciones que se pueden realizar sobre
 * los materiales en una lista persistente
 */
public class MaterialController {

    private MaterialDAO materialRepo;

    /**
     * Crea un objeto de tipo materiales controlador
     */
    public MaterialController() {
        this.materialRepo = new MaterialDAO();
    }

    /**
     * Guarda un material de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @param mtr Objeto de tipo Material que va ser almacenado
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    private void addMaterial(Material mtr) throws Exception {
        try {
            if (materialRepo.fetchMateriles(null).contains(mtr)) {
                throw new Exception("Este material ya se encuentra registrado");
            }
            materialRepo.addMaterial(mtr);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            throw new Exception(ioException.getMessage());
        }
    }

    /**
     * Guarda un material de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @param signaturaId     valor con identificaion unica del audio
     * @param fechaCompra     valor con la fecha de compra del audio
     * @param restringido     valor del estado del audio
     * @param tema            valor tema del audio
     * @param formato         valor con el formato del audio
     * @param duracionMinutos valor con la duracion en minutos del audio
     * @param idioma          valor del idioma del audio
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addAudio(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, String formato, int duracionMinutos,
                         String idioma) throws Exception {
        Audio newAudio = new Audio(signaturaId, fechaCompra, restringido, tema, Audio.EFormato.valueOf(formato.toUpperCase()),
                duracionMinutos, idioma);
        addMaterial(newAudio);
    }

    /**
     * Guarda un material de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @param signaturaId          valor la signatura id del video
     * @param fechaCompra          valor fecha de compra del video
     * @param restringido          valor estado del video
     * @param tema                 valor tema del video
     * @param audioFormato         valor formato de audio del video
     * @param duracionMinutos      valor duracion en minutos del video
     * @param idioma               valor idioma del video
     * @param nombreDirector       valor nombre del director del video
     * @param videoFormato         valor formato del video
     * @param duracionMinutosVideo valor duracion en minutos del video
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addVideo(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, String audioFormato, int duracionMinutos,
                         String idioma, String nombreDirector, String videoFormato,
                         int duracionMinutosVideo) throws Exception {
        addMaterial(
                new Video(signaturaId, fechaCompra, restringido, tema, Audio.EFormato.valueOf(audioFormato.toUpperCase()),
                        duracionMinutos, idioma, nombreDirector, Video.EFormatoVideo.valueOf(videoFormato.toUpperCase()),
                        duracionMinutosVideo)
        );
    }

    /**
     * Guarda un material de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @param signaturaId valor la signatura id de un texto
     * @param fechaCompra valor la fedha de compra del texto
     * @param restringido valor el estado del texto
     * @param tema        valor el tema del texto
     * @param titulo      valor el titulo del texto
     * @param nombreAutor valor nombre del autor del texto
     * @param fechaPublic valor la fecha de publicacion del texto
     * @param pagesNum    valor numero de paginas del texto
     * @param idioma      valor el idioma del texto
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addTextos(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema, String titulo, String nombreAutor,
                          LocalDate fechaPublic, int pagesNum, String idioma) throws Exception {
        addMaterial(
                new Textos(signaturaId, fechaCompra, restringido, tema, titulo,
                        nombreAutor, fechaPublic, pagesNum, idioma)
        );
    }

    /**
     * Guarda un material de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @param signaturaId valor signatura id de otro material
     * @param fechaCompra valor fecha de compra de otro material
     * @param restringido valor estado de otro material
     * @param tema        valor tema de otro material
     * @param descripcion valor descripcion de otro material
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addOtroMaterial(String signaturaId, LocalDate fechaCompra, boolean restringido, String tema,
                                String descripcion) throws Exception {
        addMaterial(
                new OtrosMateriales(signaturaId, fechaCompra, restringido, tema, descripcion)
        );
    }

    /**
     * Obtiene una lista de materiales en surepresentacion de tipo String
     * Ver {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @param materialType tipo de material que debera de obtener la lista
     * @return lista de materiales de tipo Material o el que se especifique
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public List<String> getAllMaterials(String materialType) throws Exception {
        return materialRepo.fetchMateriles(materialType).stream()
                .filter(Objects::nonNull)
                .map(Material::toString).collect(Collectors.toList());
    }

    /**
     * Devuelve todos los materiales alamcenados
     * Ver {@link com.jimenezzj.bibloteca.dao.MaterialDAO}
     *
     * @return lista de todos los materiales en su representación de tipo String
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public List<String> getAllMaterial() throws Exception {
        return getAllMaterials("Estudiante");
    }

}
