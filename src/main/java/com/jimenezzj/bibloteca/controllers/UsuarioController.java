package com.jimenezzj.bibloteca.controllers;

import com.jimenezzj.bibloteca.dao.UsuarioDAO;
import com.jimenezzj.bibloteca.models.Administrativo;
import com.jimenezzj.bibloteca.models.Estudiante;
import com.jimenezzj.bibloteca.models.Persona;
import com.jimenezzj.bibloteca.models.Profesor;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Representación de un objeto que maneja las acciones que se pueden realizar sobre
 * las persona en una lista persistente
 */
public class UsuarioController {

    private final UsuarioDAO userRepo;

    /**
     * Crea un objeto de tipo Usuario controlador
     */
    public UsuarioController() {
        this.userRepo = new UsuarioDAO();
    }

    /**
     * Guarda una person de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.UsuarioDAO}
     *
     * @param nuevaPersona nuevo objeto de tipo Persona que va ser almacendo
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    private void addUsuario(Persona nuevaPersona) throws Exception {
        try {
            if (userRepo.fetchPersonas(null).contains(nuevaPersona)) {
                throw new Exception("Este usuario ya se encuentra registrdo");
            }
            userRepo.addUsuario(nuevaPersona);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * Guarda una persona de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.UsuarioDAO}
     *
     * @param idCard         valor identificaion del usuario
     * @param nombre         valor nombre del usuario
     * @param primerApellido valor primer apellido del usuario
     * @param nombramiento   valor del nombramiento del usuario
     * @param horasSemana    valor horas por semana que labora el usuario
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addAdministrativo(String idCard, String nombre, String primerApellido, String nombramiento, int horasSemana)
            throws Exception {
        addUsuario(
                new Administrativo(idCard, nombre, primerApellido, nombramiento, horasSemana)
        );
    }

    /**
     * Guarda una persona de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.UsuarioDAO}
     *
     * @param idCard            valor identificaion del profesor
     * @param nombre            valor nombre del profesor
     * @param primerApellido    valor primer apellido del profesor
     * @param tipConotrato      valor tipo de contrato del profesor
     * @param fechaContratacion valor fecha de contratacion del profesor
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addProfesor(String idCard, String nombre, String primerApellido, String tipConotrato,
                            LocalDate fechaContratacion) throws Exception {
        addUsuario(
                new Profesor(idCard, nombre, primerApellido, tipConotrato, fechaContratacion)
        );
    }

    /**
     * Guarda una persona de forma persistente por medio del objeto de acceso a datos {@link com.jimenezzj.bibloteca.dao.UsuarioDAO}
     *
     * @param idCard         valor identififcaion del estuiante
     * @param nombre         valor nombre del estudiante
     * @param primerApellido valor primer apellido del estudiante
     * @param carnetId       valor carnet del estudiante
     * @param carreraCodigo  valor el codigo de la carrera que cursa
     * @param credNumActual  valor la cantidad de creditos de la carrera que cursa
     * @throws Exception excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public void addEstudiante(String idCard, String nombre, String primerApellido, String carnetId, String carreraCodigo,
                              int credNumActual) throws Exception {
        addUsuario(
                new Estudiante(idCard, nombre, primerApellido, carnetId, carreraCodigo, credNumActual)
        );
    }

    /**
     * Obtiene todos los usuarios almacenado o los que se indican
     *
     * @param typeUser tipo  de objetos que van a ser obtenidos
     * @return Lista de objetos de tipo Perona o el especificado
     * @throws IOException excepcion que ocurre cuando se ingresa un dato erroneamente
     */
    public List<String> getAllUsers(String typeUser) throws IOException {
        return userRepo.fetchPersonas(typeUser).stream()
                .filter(Objects::nonNull)
                .map(Persona::toString)
                .collect(Collectors.toList());
    }

}
