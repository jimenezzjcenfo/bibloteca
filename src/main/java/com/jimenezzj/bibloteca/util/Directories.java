package com.jimenezzj.bibloteca.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Directories {

    public static String getFilesDir() {
        return System.getProperty("user.dir") + "\\files\\";
    }

    public static List<String> getDataFromString(String word, String typeToRemove) {
        return Arrays.stream(word.replace(",", "").split("\\w+=|\\[|]|" + typeToRemove))
                .filter(w -> !w.isBlank()).map(String::trim)
                .collect(Collectors.toList());
    }

    public static List<String> getDataFromString(String word) {
        return Arrays.stream(word.replace(",", "").split("\\w+=|\\[|]"))
                .filter(w -> !w.equals("")).map(String::trim)
                .collect(Collectors.toList());
    }

    public static void writeInFile(String fileName, String dataToWrite) throws IOException {
        var p = Paths.get(Directories.getFilesDir().concat(fileName));
        try (BufferedWriter bw = Files.newBufferedWriter(p, StandardOpenOption.APPEND)) {
            bw.newLine();
            bw.write(dataToWrite);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            throw new IOException(ioException.getMessage());
        }
    }

}
